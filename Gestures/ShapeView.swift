//
//  ShapeView.swift
//  Gestures
//
//  Created by Victor Kachalov on 12.10.17.
//  Copyright © 2017 Victor Kachalov. All rights reserved.
//

import UIKit

class ShapeView: UIView {

    let size: CGFloat = 150
    let lineWidth: CGFloat = 3.0
    
    
    init(origin: CGPoint) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: size, height: size))
        center = origin
        backgroundColor = .clear
        
        initGestureRecognizer()
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //кривая Безье - сглаживание ломанных линий
    
    override func draw(_ rect: CGRect) {
        
        let insetRect = rect.insetBy(dx: lineWidth / 2, dy: lineWidth / 2)
        let path = UIBezierPath(roundedRect: insetRect, cornerRadius: 10)
        
        //для конкретной кривой Безье
//        let path2 = UIBezierPath()
//        path2.move(to: CGPoint)
//        path2.addLine(to: CGPoint)
//        path2.addLine(to: CGPoint)
//        path2.addCurve(to: CGPoint, controlPoint1: CGPoint, controlPoint2: CGPoint)
        
        randomColor().setFill()
        path.fill()
        path.lineWidth = lineWidth
        UIColor.black.setStroke() //цвет обводки
        path.stroke()
        
    }
    
    
    func randomColor() -> UIColor {
        
        let hue: CGFloat = CGFloat(Float(arc4random()) / Float(UInt32.max))
        return UIColor(hue: hue, saturation: 0.8, brightness: 1, alpha: 0.8) // оттенок/насыщенность/яркость/прозрачность
        
    }
    
    
    
    func initGestureRecognizer() {
        
        let panGR = UIPanGestureRecognizer(target: self, action: #selector(didPan))
        addGestureRecognizer(panGR)
        
        let pinchGR = UIPinchGestureRecognizer(target: self, action: #selector(didPinch))
        addGestureRecognizer(pinchGR)
        
        let rotationGR = UIRotationGestureRecognizer(target: self, action: #selector(didRotate))
        addGestureRecognizer(rotationGR)
        
    }
    
    
    @objc func didPan(panGR: UIPanGestureRecognizer) {
        
        superview?.bringSubview(toFront: self)
        
        var translation = panGR.translation(in: self)
        translation = translation.applying(transform)
        
        center.x += translation.x
        center.y += translation.y
        
        panGR.setTranslation(CGPoint.zero, in: self)
        
    }
    
    
    @objc func didPinch(pinchGR: UIPinchGestureRecognizer) {
        
        superview?.bringSubview(toFront: self)
        
        let scale = pinchGR.scale
        
        transform = transform.scaledBy(x: scale, y: scale)
        
        pinchGR.scale = 1
        
    }
    
    
    @objc func didRotate(rotationGR: UIRotationGestureRecognizer) {
        
        superview?.bringSubview(toFront: self)
        
        let rotation = rotationGR.rotation
        
        transform = transform.rotated(by: rotation)
        
        rotationGR.rotation = 0
        
    }
    
}
