//
//  ViewController.swift
//  Gestures
//
//  Created by Victor Kachalov on 12.10.17.
//  Copyright © 2017 Victor Kachalov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapped))
        view.addGestureRecognizer(tap)
    }
    
    
    
    @objc func didTapped(tap: UITapGestureRecognizer) {
        
        let tapPoint = tap.location(in: view)
        let shapeView = ShapeView(origin: tapPoint)
        
        view.addSubview(shapeView)
        
    }

    
    
}

